from src import archive
import matplotlib.pyplot as plt


def getlist(method, intervals, f, a, lower, upper, ref_val):#Получение разницы между абсолютным значением и подсчитанным
    lst = []
    for i in intervals:
        cur_val = method(f, a, lower, upper, i)
        lst.append(abs(ref_val - cur_val))
    return lst

value = 86.23346 #Список данных, чтобы вручную не вводить
a = 3
lower = 15
upper = 45

plotting = [i for i in range(100000, 1000000, 100000)]#Массив на большое количество интервалов - больше, лучше видно разницу

trapezoid = getlist(archive.trapezoid, plotting, archive.G, a, lower, upper, value)
rectangle = getlist(archive.rectangle, plotting, archive.G, a, lower, upper, value)
simpson = getlist(archive.simpson, plotting, archive.G, a, lower, upper, value)
#Вывод графиков
plt.figure(figsize=(9, 6))
plt.plot(plotting, trapezoid, '-.', label='Метод трапеций')
plt.plot(plotting, rectangle, ':', label='Метод прямоугольников')
plt.plot(plotting, simpson, 'y', label='Метод Симпсона')
#Подпись осей графиков
plt.xlabel('Кол-во интервалов')
plt.ylabel('Разница приближ. и истин.')
plt.legend()
plt.grid()
plt.show()

